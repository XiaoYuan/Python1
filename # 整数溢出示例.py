# 整数溢出示例

# 最大的整数值 (在大多数系统上)
max_int_value = 2**31 - 1

# 增加一个超过最大值的数
overflowed_value = input()
a = max_int_value + overflowed_value

print("最大整数值:", max_int_value)
print("溢出后的值:", overflowed_value)
