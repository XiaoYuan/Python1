#  导入弹窗模块enterbox   
# 也可以使用input收集输入的数据

# 写出判读一个四位数分别相加等于该输入的数字
#  is_four_leaf_number   变量名
def is_four_leaf_number(num):
    # 把输入的数字 转换成字符串遍历出来，拿到每一个字符
    target = [int(x) for x in str(num)]
    # 打印转换后的目标数字
    print(target)

    # 重要判断条件   单个数字的四次方利用sum() 求和 和输入目标数字作比较
    if sum([x ** 4 for x in target]) == num:
        return True  #如果相等 说明条件成立
    else:
        return False #如果不相等 说明条件不成立

# 输入一个四位数
num = int(input("请输入一个四位数: "))

if is_four_leaf_number(num):  
    print("Y")
else:
    print("N")